package com.example.calculadoras;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CalculadoraExponencial extends AppCompatActivity implements View.OnClickListener{

    EditText base, expo;
    Button calcular;
    TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora_exponencial);

        base = findViewById(R.id.base);
        expo = findViewById(R.id.expo);

        calcular = findViewById(R.id.calcul);

        resultado = findViewById(R.id.resultado);

        calcular.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String b = base.getText().toString();
        String e = expo.getText().toString();

        int base = Integer.parseInt(b);
        int exponente = Integer.parseInt(e);

        int rta = 0;

        rta = (int)Math.pow(base, exponente);

        resultado.setText(""+rta);
    }
}
