package com.example.calculadoras;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ReglaDeTres extends AppCompatActivity implements View.OnClickListener{

    EditText num1, num2, num3;
    Button calcular;
    TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regla_de_tres);

        num1 = findViewById(R.id.num1);
        num2 = findViewById(R.id.num2);
        num3 = findViewById(R.id.num3);

        calcular = findViewById(R.id.calcul);

        resultado = findViewById(R.id.resultado);

        calcular.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String n1 = num1.getText().toString();
        String n2 = num2.getText().toString();
        String n3 = num3.getText().toString();

        int num1 = Integer.parseInt(n1);
        int num2 = Integer.parseInt(n2);
        int num3 = Integer.parseInt(n3);

        int rta = 0;

        rta =(num3*num2)/num1;

        resultado.setText(""+rta);
    }
}